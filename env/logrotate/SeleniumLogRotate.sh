#!/bin/sh
#Log Rotation Script AM 0:00

# ログファイルが置かれているフォルダ
LOGDIR="/var/log"
# ログファイルを保管するフォルダ(今は同じフォルダに保管してる)
OLDLOGDIR="/var/log"
# ローテーション対象ログ(CakePHPが管理しているログは対象外)
targets="selenium.log"

sleep 1

DATE=`date +%Y%m%d --date '1 day ago'`

for file in $targets
do
    /bin/cp ${LOGDIR}/${file} ${OLDLOGDIR}/${file}.$DATE

    if [ $? = 0 ];then
        /bin/cp /dev/null ${LOGDIR}/${file}
        # 必要なら個々でgzipなどする
    fi

    # delete log file before 6 days
    # 削除対象が存在しない時のために --no-run-if-empty オプションを指定
    /usr/bin/find ${OLDLOGDIR}/${file}.* -mtime +5 | xargs --no-run-if-empty /bin/rm

   echo "finish rotate ${file}"
done