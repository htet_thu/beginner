#!/bin/sh
# Remove Chrome cache Script AM 0:00

# キャッシュファイルが置かれているフォルダ
DIR="/tmp"

# キャッシュを残しておく日数
HOLDDAY=2

# 検索するフォルダ
GREP=".com.google.Chrome"


for cachefile in $(/bin/ls -a ${DIR} | grep ${GREP}); do
#    echo "-- Debug start --"
#    echo "${DIR}/${cachefile}"
#    echo $(($(date +%s) - $(stat -c %Y "${DIR}/${cachefile}")))
#    echo $((60 * 60 * 24 * ${HOLDDAY}))
#    echo "-- Debug end --"
    if [ $(($(/bin/date +%s) - $(/bin/stat -c %Y "${DIR}/${cachefile}"))) -gt $((60 * 60 * 24 * ${HOLDDAY})) ];then
        echo "finish remove ${DIR}/${cachefile}"
        /bin/rm -rf "${DIR}/${cachefile}"
    fi
done