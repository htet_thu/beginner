sudo yum -y install \
    unzip \
    wget

#Java8 install
sudo yum install -y java-1.8.0-openjdk

#Xvfb install
sudo yum install -y xorg-x11-server-Xvfb

#Xvfb 自動起動設定＆起動
#起動スクリプトテンプレート
sudo wget  https://raw.githubusercontent.com/kohkimakimoto/chef-cookbooks-selenium/master/templates/default/init.d/xvfb.erb
sudo mv xvfb.erb /etc/init.d/Xvfb
sudo chmod 755 /etc/init.d/Xvfb
sudo chkconfig Xvfb on
sudo systemctl start Xvfb
#※DISPLAY番号は:99となっている。

#Chrome install(v63)
sudo bash -c 'cat << EOF > /etc/yum.repos.d/google-chrome.repo
[google-chrome]
name=google-chrome - \$basearch
baseurl=http://dl.google.com/linux/chrome/rpm/stable/\$basearch
enabled=1
gpgcheck=1
gpgkey=https://dl-ssl.google.com/linux/linux_signing_key.pub
EOF'
sudo yum install -y google-chrome-stable

#Chrome driver download
#wget https://chromedriver.storage.googleapis.com/2.34/chromedriver_linux64.zip
wget https://chromedriver.storage.googleapis.com/2.40/chromedriver_linux64.zip
unzip chromedriver*
sudo mv chromedriver /usr/local/bin
sudo chmod 755 /usr/local/bin/chromedriver
sudo rm chromedriver*

#-----------------------------------------------
#seleniumをサービス化する
#-----------------------------------------------

#create group
groupname=selenium
username=selusr
egrep -i "^$groupname:" /etc/group;
if (( $? > 0 )); then
   sudo groupadd $groupname
fi
#create user
egrep -i "^$username:" /etc/passwd;
if (( $? > 0 )); then
   sudo useradd $username
fi
#add user to group
sudo usermod -aG $groupname $username

#create log file
sudo touch /var/log/selenium.log
sudo chmod 777 /var/log/selenium.log

#copy boot files to local
selenium_path=/usr/local/share/selenium-server-standalone-3.8.1.jar
boot_path=/usr/local/bin/selenium-start.sh
sudo cp /var/www/html/src/env/selenium/selenium-server-standalone-3.8.1.jar $selenium_path
sudo chmod 755 $selenium_path
sudo cp /var/www/html/src/env/selenium/start.sh $boot_path
sudo chmod 755 $boot_path

#create selenium service
sudo cp /var/www/html/src/env/selenium/selenium.service /etc/systemd/system/selenium.service
sudo chmod 755 /etc/systemd/system/selenium.service
sudo systemctl daemon-reload
sudo systemctl enable selenium
sudo systemctl start selenium


#Selenium download
#http://www.seleniumhq.org/download/selenium-server-standalone-3.8.1.jar をダウンロード
#※/var/www/src/env/selenium の下に配置済み

#Selenium 起動
#DISPLAY=:99 xvfb-run java -jar /var/www/html/src/env/selenium/selenium-server-standalone-3.8.1.jar -enablePassThrough false > /dev/null &

#動作確認
# $ cd /var/www/html/src/app
# $ bin/cake webdriver_test