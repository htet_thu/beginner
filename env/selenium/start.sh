#!/bin/bash

export DISPLAY=:99
java -jar /usr/local/share/selenium-server-standalone-3.8.1.jar -enablePassThrough false |& tee -a /var/log/selenium.log
