#!/usr/bin/env bash

if ! [ `which ansible` ]; then
	yum install -y ansible
fi

ansible-playbook --private-key=.vagrant/machines/crawler/virtualbox/private_key -u vagrant -i /var/www/html/env/ansible/inventories/web /var/www/html/env/ansible/web.yml

#restart apache
sudo systemctl restart httpd

cd /var/www/html/
#composer install